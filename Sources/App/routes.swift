import Crypto
import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    // basic / password auth protected routes
    let userBasicRouter = router.grouped(User.basicAuthMiddleware(using: BCryptDigest()))
    let deviceBasicRouter = router.grouped(Device.basicAuthMiddleware(using: BCryptDigest()))
    // bearer / token auth protected routes
    let userBearerRouter = router.grouped(User.tokenAuthMiddleware())
    let deviceBearerRouter = router.grouped(Device.tokenAuthMiddleware())

    // MARK: - Users
    
    let userController = UserController()
    
    router.post("users", "register", use: userController.create)
    userBasicRouter.post("users", "login", use: userController.login)
    userBearerRouter.get("users", use: userController.users)
//    bearer.post("users/logout", use: userController.logout)
    
//    let todoController = TodoController()
//    bearer.get("todos", use: todoController.index)
//    bearer.post("todos", use: todoController.create)
//    bearer.delete("todos", Todo.parameter, use: todoController.delete)
    
    // MARK: - Device
    
    let deviceController = DeviceController()
    
    router.post("devices", "register", use: deviceController.create)
    router.post("devices", "login", use: deviceController.login)
    userBearerRouter.post("devices", "privatize", Int.parameter, use: deviceController.privatize)

    // MARK: - Statistic
    
    // MARK: - Model
    
    let modelController = ModelController()
    
    deviceBearerRouter.get("model", Int.parameter, use: modelController.index)
    
    // MARK: - Showings
    
    let showingController = ShowingController()
    deviceBearerRouter.post("showing", Int.parameter, use: showingController.create)
    userBearerRouter.get("showings", Int.parameter, use: showingController.index)
    
}
