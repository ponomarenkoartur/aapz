//
//  Device.swift
//  App
//
//  Created by Artur on 5/5/19.
//

import Authentication
import FluentSQLite
import Vapor

/// A registered user, capable of owning todo items.
final class Device: SQLiteModel {
    /// User's unique identifier.
    /// Can be `nil` if the user has not been saved yet.
    var id: Int?
    
    /// User's full name.
    var macAddress: String
    
    /// BCrypt hash of the user's password.
    var passwordHash: String
    
    var userID: Int?
    
    /// Creates a new `User`.
    init(id: Int? = nil, macAddress: String, passwordHash: String, userID: Int? = nil) {
        self.id = id
        self.macAddress = macAddress
        self.passwordHash = passwordHash
        self.userID = userID
    }
}

/// Allows users to be verified by basic / password auth middleware.
extension Device: PasswordAuthenticatable {
    /// See `PasswordAuthenticatable`.
    static var usernameKey: WritableKeyPath<Device, String> {
        return \.macAddress
    }
    
    /// See `PasswordAuthenticatable`.
    static var passwordKey: WritableKeyPath<Device, String> {
        return \.passwordHash
    }
}

/// Allows users to be verified by bearer / token auth middleware.
extension Device: TokenAuthenticatable {
    typealias TokenType = DeviceToken
}

/// Allows `User` to be used as a Fluent migration.
extension Device: Migration {
    /// See `Migration`.
    static func prepare(on conn: SQLiteConnection) -> Future<Void> {
        return SQLiteDatabase.create(Device.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.macAddress)
            builder.field(for: \.passwordHash)
            builder.field(for: \.userID)
            builder.unique(on: \.macAddress)
            builder.reference(from: \.userID, to: \User.id)
        }
    }
}

/// Allows `User` to be encoded to and decoded from HTTP messages.
extension Device: Content { }

/// Allows `User` to be used as a dynamic parameter in route definitions.
extension Device: Parameter { }
