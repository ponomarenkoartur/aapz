//
//  Showing.swift
//  App
//
//  Created by Artur on 5/5/19.
//

import FluentSQLite
import Vapor

final class Showing: SQLiteModel {
    
    var id: Int?
    var time: Date?
    var pulse: Int?
    var temperature: Int?
    var userID: User.ID
    var deviceID: Device.ID
    
    init(id: Int? = nil,
         time: Date?,
         pulse: Int?,
         temperature: Int?,
         userID: User.ID,
         deviceID: Device.ID) {
        self.id = id
        self.time = time
        self.pulse = pulse
        self.temperature = temperature
        self.userID = userID
        self.deviceID = deviceID
    }
}

extension Showing {
    var user: Parent<Showing, User> {
        return parent(\.userID)
    }
    
    var device: Parent<Showing, Device> {
        return parent(\.deviceID)
    }
}

extension Showing: Migration {
    static func prepare(on conn: SQLiteConnection) -> Future<Void> {
        return SQLiteDatabase.create(Showing.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.time)
            builder.field(for: \.pulse)
            builder.field(for: \.temperature)
            builder.field(for: \.userID)
            builder.field(for: \.deviceID)
            builder.reference(from: \.userID, to: \User.id)
            builder.reference(from: \.deviceID, to: \Device.id)
        }
    }
}

extension Showing: Content { }

extension Showing: Parameter { }

