//
//  DeviceToken.swift
//  App
//
//  Created by Artur on 5/6/19.
//

import Authentication
import Crypto
import FluentSQLite
import Vapor

/// An ephermal authentication token that identifies a registered device.
final class DeviceToken: SQLiteModel {
    /// Creates a new `DeviceToken` for a given device.
    static func create(deviceID: Device.ID) throws -> DeviceToken {
        // generate a random 128-bit, base64-encoded string.
        let string = try CryptoRandom().generateData(count: 16).base64EncodedString()
        // init a new `DeviceToken` from that string.
        return .init(string: string, deviceID: deviceID)
    }
    
    /// See `Model`.
    static var deletedAtKey: TimestampKey? { return \.expiresAt }
    
    /// DeviceToken's unique identifier.
    var id: Int?
    
    /// Unique token string.
    var string: String
    
    /// Reference to device that owns this token.
    var deviceID: Device.ID
    
    /// Expiration date. Token will no longer be valid after this point.
    var expiresAt: Date?
    
    /// Creates a new `DeviceToken`.
    init(id: Int? = nil, string: String, deviceID: Device.ID) {
        self.id = id
        self.string = string
        // set token to expire after 5 hours
        self.expiresAt = Date.init(timeInterval: 60 * 60 * 5, since: .init())
        self.deviceID = deviceID
    }
}

extension DeviceToken {
    /// Fluent relation to the device that owns this token.
    var device: Parent<DeviceToken, Device> {
        return parent(\.deviceID)
    }
}

/// Allows this model to be used as a TokenAuthenticatable's token.
extension DeviceToken: Token {
    /// See `Token`.
    typealias UserType = Device
    
    /// See `Token`.
    static var tokenKey: WritableKeyPath<DeviceToken, String> {
        return \.string
    }
    
    /// See `Token`.
    static var userIDKey: WritableKeyPath<DeviceToken, Device.ID> {
        return \.deviceID
    }
}

/// Allows `DeviceToken` to be used as a Fluent migration.
extension DeviceToken: Migration {
    /// See `Migration`.
    static func prepare(on conn: SQLiteConnection) -> Future<Void> {
        return SQLiteDatabase.create(DeviceToken.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.string)
            builder.field(for: \.deviceID)
            builder.field(for: \.expiresAt)
            builder.reference(from: \.deviceID, to: \Device.id)
        }
    }
}

/// Allows `DeviceToken` to be encoded to and decoded from HTTP messages.
extension DeviceToken: Content { }

/// Allows `DeviceToken` to be used as a dynamic parameter in route definitions.
extension DeviceToken: Parameter { }

