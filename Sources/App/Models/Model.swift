//
//  Model.swift
//  App
//
//  Created by Artur on 5/5/19.
//

import FluentSQLite
import Vapor

/// A single entry of a todo list.
final class Model: SQLiteModel {

    var id: Int?
    var pulse: Int
    var temperature: Double
    var maxTemperatureDifferencePerMinute: Double
    var maxPulseDifferencePerMinute: Double
    var userID: User.ID
    
    /// Creates a new `Todo`.
    init(id: Int? = nil,
         pulse: Int,
         temperature: Double,
         maxTemperatureDifferencePerMinute: Double,
         maxPulseDifferencePerMinute: Double,
         userID: User.ID) {
        self.pulse = pulse
        self.temperature = temperature
        self.maxTemperatureDifferencePerMinute = maxTemperatureDifferencePerMinute
        self.maxPulseDifferencePerMinute = maxPulseDifferencePerMinute
        self.userID = userID
    }
}

extension Model {
    /// Fluent relation to user that owns this todo.
    var user: Parent<Model, User> {
        return parent(\.userID)
    }
}

/// Allows `Todo` to be used as a Fluent migration.
extension Model: Migration {
    static func prepare(on conn: SQLiteConnection) -> Future<Void> {
        return SQLiteDatabase.create(Model.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.userID)
            builder.field(for: \.pulse)
            builder.field(for: \.temperature)
            builder.field(for: \.maxTemperatureDifferencePerMinute)
            builder.field(for: \.maxPulseDifferencePerMinute)
            builder.reference(from: \.userID, to: \User.id)
        }
    }
}

extension Model: Content { }

extension Model: Parameter { }
