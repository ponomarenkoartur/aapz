//
//  DeviceController.swift
//  App
//
//  Created by Artur on 5/5/19.
//

import Vapor
import FluentSQLite
import Crypto

final class DeviceController {
//    func index(_ req: Request) throws -> Future<[Device]> {
//        let user = try req.requireAuthenticated(User.self)
//
//        return try Device.query(on: req).all()
////            .filter(\.userID == user.requireID()).all()
//    }
    
    func create(_ req: Request) throws -> Future<DeviceResponse> {
        return try req.content.decode(CreateDeviceRequest.self).flatMap { device -> Future<Device> in
            // hash user's password using BCrypt
            let hash = try BCrypt.hash(device.password)
            // save new user
            return Device.init(macAddress: device.macAddress, passwordHash: hash)
                .save(on: req)
            }.map { device in
                // map to public user response (omits password hash)
                return try DeviceResponse(id: device.requireID(), macAddress: device.macAddress)
        }
    }
    
    func login(_ req: Request) throws -> Future<DeviceToken> {

        // get user auth'd by basic auth middleware
        let device = try req.requireAuthenticated(Device.self)

        // create new token for this user
        let token = try DeviceToken.create(deviceID: device.requireID())

        return token.save(on: req)
    }
    
    func privatize(_ req: Request) throws -> Future<DeviceToken> {
        // TODO: Implement function
        let device = try req.requireAuthenticated(Device.self)
        let token = try DeviceToken.create(deviceID: device.requireID())
        return token.save(on: req)
    }
}

// MARK: Content

struct CreateDeviceRequest: Content {
    var macAddress: String
    var password: String
}


/// Public representation of user data.
struct DeviceResponse: Content {
    var id: Int
    var macAddress: String
}
