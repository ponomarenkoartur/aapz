//
//  ShowingController.swift
//  App
//
//  Created by Artur on 5/5/19.
//

import Vapor
import FluentSQLite

final class ShowingController {
    func index(_ req: Request) throws -> Future<[Showing]> {
        let user = try req.requireAuthenticated(User.self)
        
        return try Showing.query(on: req)
            .filter(\.userID == user.requireID()).all()
    }
    
    func create(_ req: Request) throws -> Future<Showing> {
        let device = try req.requireAuthenticated(Device.self)
        
        return try req.content.decode(CreateShowingRequest.self).flatMap { showing in
            Device.query(on: req)
                .filter(\.userID == showing.userID)
                .first()
                .map({ usersDevice in
                    guard let usersDevice = usersDevice,
                        try usersDevice.id  == device.requireID() else {
                            throw Abort(.forbidden)
                    }
                })
            
            return try Showing(time: showing.time, pulse: showing.pulse, temperature: showing.temperature, userID: showing.userID!, deviceID: device.requireID())
                .save(on: req)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        let user = try req.requireAuthenticated(User.self)
        
        return try req.parameters.next(Showing.self).flatMap { showing -> Future<Void> in
            guard try showing.userID == user.requireID() else {
                throw Abort(.forbidden)
            }

            return showing.delete(on: req)
            }.transform(to: .ok)
    }
}

// MARK: Content

struct CreateShowingRequest: Content {
    var time: Date?
    var pulse: Int?
    var temperature: Int?
    var userID: Int?
}
