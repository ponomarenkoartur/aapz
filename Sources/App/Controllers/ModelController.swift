//
//  ModelController.swift
//  App
//
//  Created by Artur on 5/5/19.
//

import Vapor
import FluentSQLite

final class ModelController {
    func index(_ req: Request) throws -> Future<[Model]> {
        let user = try req.requireAuthenticated(User.self)
        
        return try Model.query(on: req)
            .filter(\.userID == user.requireID()).all()
    }
    
    func create(_ req: Request) throws -> Future<Model> {
        let user = try req.requireAuthenticated(User.self)
        return try req.content.decode(CreateModelRequest.self).flatMap { model in
            return try Model(
                pulse: model.pulse,
                temperature: model.temperature,
                maxTemperatureDifferencePerMinute: model.maxTemperatureDifferencePerMinute,
                maxPulseDifferencePerMinute: model.maxPulseDifferencePerMinute,
                userID: user.requireID())
                .save(on: req)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        let user = try req.requireAuthenticated(User.self)
        
        return try req.parameters.next(Model.self).flatMap { model -> Future<Void> in
            guard try model.userID == user.requireID() else {
                throw Abort(.forbidden)
            }
        
            return model.delete(on: req)
            }.transform(to: .ok)
    }
}

// MARK: Content

struct CreateModelRequest: Content {
    var pulse: Int
    var temperature: Double
    var maxTemperatureDifferencePerMinute: Double
    var maxPulseDifferencePerMinute: Double
}
